using System;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Identity;

namespace BusLine.Models
{
    public class BusLineUser : IdentityUser{
    public string FirstName { get; set;}
    public string LastName { get; set;}
	}

    public class FromPoint{
		public int FromPointID {get; set;}
		public string FromStation {get; set;}
	}
	public class ToPoint{
		public int ToPointID {get; set;}
		public string ToStation {get; set;}
	}
	
	public class Booking
	{
		public int BookingId {get; set;}
		
		public int FromPointID {get; set;}
		public FromPoint FromSt {get; set;}
		
		public int ToPointID {get; set;}
		public ToPoint ToSt {get; set;}
		
		[DataType(DataType.Date)]
		public DateTime DapartureDate {get; set;}
		public int SeatNumber {get; set;}
		
		public string CarType {get; set;}

		public string BusLineUserID{get; set;}
		public BusLineUser postUser{get; set;}
	}

}