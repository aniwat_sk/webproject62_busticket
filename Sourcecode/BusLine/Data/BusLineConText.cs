using BusLine.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;

namespace BusLine.Data
{
	public class BusLineContext : IdentityDbContext<BusLineUser>
	{
		public DbSet<Booking> BookTicket { get; set; }

		protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
		{
			optionsBuilder.UseSqlite(@"Data source=BusLine.db");
		}

		public DbSet<BusLine.Models.FromPoint> FromPoint { get; set; }

		public DbSet<BusLine.Models.ToPoint> ToPoint { get; set; }
	}
}
