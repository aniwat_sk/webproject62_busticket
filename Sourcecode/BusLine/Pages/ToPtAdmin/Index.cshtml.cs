using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using BusLine.Data;
using BusLine.Models;

namespace BusLine.Pages.ToPtAdmin
{
    public class IndexModel : PageModel
    {
        private readonly BusLine.Data.BusLineContext _context;

        public IndexModel(BusLine.Data.BusLineContext context)
        {
            _context = context;
        }

        public IList<ToPoint> ToPoint { get;set; }

        public async Task OnGetAsync()
        {
            ToPoint = await _context.ToPoint.ToListAsync();
        }
    }
}
