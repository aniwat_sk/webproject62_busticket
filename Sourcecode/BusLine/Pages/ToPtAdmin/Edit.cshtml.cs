using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using BusLine.Data;
using BusLine.Models;

namespace BusLine.Pages.ToPtAdmin
{
    public class EditModel : PageModel
    {
        private readonly BusLine.Data.BusLineContext _context;

        public EditModel(BusLine.Data.BusLineContext context)
        {
            _context = context;
        }

        [BindProperty]
        public ToPoint ToPoint { get; set; }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            ToPoint = await _context.ToPoint.FirstOrDefaultAsync(m => m.ToPointID == id);

            if (ToPoint == null)
            {
                return NotFound();
            }
            return Page();
        }

        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            _context.Attach(ToPoint).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ToPointExists(ToPoint.ToPointID))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return RedirectToPage("./Index");
        }

        private bool ToPointExists(int id)
        {
            return _context.ToPoint.Any(e => e.ToPointID == id);
        }
    }
}
