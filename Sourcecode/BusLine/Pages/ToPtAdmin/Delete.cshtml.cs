using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using BusLine.Data;
using BusLine.Models;

namespace BusLine.Pages.ToPtAdmin
{
    public class DeleteModel : PageModel
    {
        private readonly BusLine.Data.BusLineContext _context;

        public DeleteModel(BusLine.Data.BusLineContext context)
        {
            _context = context;
        }

        [BindProperty]
        public ToPoint ToPoint { get; set; }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            ToPoint = await _context.ToPoint.FirstOrDefaultAsync(m => m.ToPointID == id);

            if (ToPoint == null)
            {
                return NotFound();
            }
            return Page();
        }

        public async Task<IActionResult> OnPostAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            ToPoint = await _context.ToPoint.FindAsync(id);

            if (ToPoint != null)
            {
                _context.ToPoint.Remove(ToPoint);
                await _context.SaveChangesAsync();
            }

            return RedirectToPage("./Index");
        }
    }
}
