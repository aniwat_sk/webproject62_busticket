using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using BusLine.Data;
using BusLine.Models;

namespace BusLine.Pages.BookingAdmin
{
    public class DeleteModel : PageModel
    {
        private readonly BusLine.Data.BusLineContext _context;

        public DeleteModel(BusLine.Data.BusLineContext context)
        {
            _context = context;
        }

        [BindProperty]
        public Booking Booking { get; set; }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            Booking = await _context.BookTicket
                .Include(b => b.FromSt)
                .Include(b => b.ToSt)
                .Include(b => b.postUser).FirstOrDefaultAsync(m => m.BookingId == id);

            if (Booking == null)
            {
                return NotFound();
            }
            return Page();
        }

        public async Task<IActionResult> OnPostAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            Booking = await _context.BookTicket.FindAsync(id);

            if (Booking != null)
            {
                _context.BookTicket.Remove(Booking);
                await _context.SaveChangesAsync();
            }

            return RedirectToPage("./Index");
        }
    }
}
