using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using BusLine.Data;
using BusLine.Models;

namespace BusLine.Pages.BookingAdmin
{
    public class CreateModel : PageModel
    {
        private readonly BusLine.Data.BusLineContext _context;

        public CreateModel(BusLine.Data.BusLineContext context)
        {
            _context = context;
        }

        public IActionResult OnGet()
        {
        ViewData["FromPointID"] = new SelectList(_context.Set<FromPoint>(), "FromPointID", "FromStation");
        ViewData["ToPointID"] = new SelectList(_context.Set<ToPoint>(), "ToPointID", "ToStation");
        ViewData["BusLineUserID"] = new SelectList(_context.Users, "Id", "Id");
            return Page();
        }

        [BindProperty]
        public Booking Booking { get; set; }

        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            _context.BookTicket.Add(Booking);
            await _context.SaveChangesAsync();

            return RedirectToPage("./Index");
        }
    }
}