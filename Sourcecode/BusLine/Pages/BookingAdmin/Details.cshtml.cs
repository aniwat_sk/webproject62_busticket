using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using BusLine.Data;
using BusLine.Models;

namespace BusLine.Pages.BookingAdmin
{
    public class DetailsModel : PageModel
    {
        private readonly BusLine.Data.BusLineContext _context;

        public DetailsModel(BusLine.Data.BusLineContext context)
        {
            _context = context;
        }

        public Booking Booking { get; set; }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            Booking = await _context.BookTicket
                .Include(b => b.FromSt)
                .Include(b => b.ToSt)
                .Include(b => b.postUser).FirstOrDefaultAsync(m => m.BookingId == id);

            if (Booking == null)
            {
                return NotFound();
            }
            return Page();
        }
    }
}
