using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using BusLine.Data;
using BusLine.Models;

namespace BusLine.Pages.BookingAdmin
{
    public class IndexModel : PageModel
    {
        private readonly BusLine.Data.BusLineContext _context;

        public IndexModel(BusLine.Data.BusLineContext context)
        {
            _context = context;
        }

        public IList<Booking> Booking { get;set; }

        public async Task OnGetAsync()
        {
            Booking = await _context.BookTicket
                .Include(b => b.FromSt)
                .Include(b => b.ToSt)
                .Include(b => b.postUser).ToListAsync();
        }
    }
}
