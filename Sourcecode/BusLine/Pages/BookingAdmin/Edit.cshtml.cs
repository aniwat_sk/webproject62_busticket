using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using BusLine.Data;
using BusLine.Models;

namespace BusLine.Pages.BookingAdmin
{
    public class EditModel : PageModel
    {
        private readonly BusLine.Data.BusLineContext _context;

        public EditModel(BusLine.Data.BusLineContext context)
        {
            _context = context;
        }

        [BindProperty]
        public Booking Booking { get; set; }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            Booking = await _context.BookTicket
                .Include(b => b.FromSt)
                .Include(b => b.ToSt)
                .Include(b => b.postUser).FirstOrDefaultAsync(m => m.BookingId == id);

            if (Booking == null)
            {
                return NotFound();
            }
           ViewData["FromPointID"] = new SelectList(_context.Set<FromPoint>(), "FromPointID", "FromStation");
           ViewData["ToPointID"] = new SelectList(_context.Set<ToPoint>(), "ToPointID", "ToStation");
           ViewData["BusLineUserID"] = new SelectList(_context.Users, "Id", "Id");
            return Page();
        }

        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            _context.Attach(Booking).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!BookingExists(Booking.BookingId))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return RedirectToPage("./Index");
        }

        private bool BookingExists(int id)
        {
            return _context.BookTicket.Any(e => e.BookingId == id);
        }
    }
}
