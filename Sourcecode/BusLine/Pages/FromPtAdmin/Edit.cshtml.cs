using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using BusLine.Data;
using BusLine.Models;

namespace BusLine.Pages.FromPtAdmin
{
    public class EditModel : PageModel
    {
        private readonly BusLine.Data.BusLineContext _context;

        public EditModel(BusLine.Data.BusLineContext context)
        {
            _context = context;
        }

        [BindProperty]
        public FromPoint FromPoint { get; set; }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            FromPoint = await _context.FromPoint.FirstOrDefaultAsync(m => m.FromPointID == id);

            if (FromPoint == null)
            {
                return NotFound();
            }
            return Page();
        }

        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            _context.Attach(FromPoint).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!FromPointExists(FromPoint.FromPointID))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return RedirectToPage("./Index");
        }

        private bool FromPointExists(int id)
        {
            return _context.FromPoint.Any(e => e.FromPointID == id);
        }
    }
}
