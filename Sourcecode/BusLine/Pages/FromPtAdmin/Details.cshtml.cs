using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using BusLine.Data;
using BusLine.Models;

namespace BusLine.Pages.FromPtAdmin
{
    public class DetailsModel : PageModel
    {
        private readonly BusLine.Data.BusLineContext _context;

        public DetailsModel(BusLine.Data.BusLineContext context)
        {
            _context = context;
        }

        public FromPoint FromPoint { get; set; }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            FromPoint = await _context.FromPoint.FirstOrDefaultAsync(m => m.FromPointID == id);

            if (FromPoint == null)
            {
                return NotFound();
            }
            return Page();
        }
    }
}
