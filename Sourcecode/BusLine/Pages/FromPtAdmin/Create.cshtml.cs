using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using BusLine.Data;
using BusLine.Models;

namespace BusLine.Pages.FromPtAdmin
{
    public class CreateModel : PageModel
    {
        private readonly BusLine.Data.BusLineContext _context;

        public CreateModel(BusLine.Data.BusLineContext context)
        {
            _context = context;
        }

        public IActionResult OnGet()
        {
            return Page();
        }

        [BindProperty]
        public FromPoint FromPoint { get; set; }

        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            _context.FromPoint.Add(FromPoint);
            await _context.SaveChangesAsync();

            return RedirectToPage("./Index");
        }
    }
}